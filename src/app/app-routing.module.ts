import { NgModule } from '@angular/core';
import {PreloadAllModules, Router, RouterModule, Routes} from '@angular/router';
import {MoviesDetailComponent} from './movies-detail.component';

const routes: Routes = [
  { path: '', redirectTo: 'movies', pathMatch: 'full' },
  { path: 'movies', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'movie', loadChildren: './movie/movie.module#MoviePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }, )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(private router: Router) {}

  navigate() {
    this.router.navigate(['./movies-detail.component.html']);
  }
}
