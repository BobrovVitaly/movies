import { Component, OnInit } from '@angular/core';
import {Movie} from './movie';

@Component({
  selector: 'app-movies-detail',
  templateUrl: './movies-detail.component.html',
  styleUrls: ['./movies-detail.component.scss'],
})
export class MoviesDetailComponent implements OnInit {
  movie: Movie;

  ngOnInit() {}

}
