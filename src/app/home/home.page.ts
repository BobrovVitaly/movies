import {Component, OnInit} from '@angular/core';
import {Movie} from '../movie';
import {MoviesService} from '../movies.service';
import {Router} from '@angular/router';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    constructor(
        private navCtrl: NavController,
        private router: Router,
        private moviesService: MoviesService,
    ) {
    }

    public films: Movie[];
    public deleteId: number = undefined;
    public addId: number = undefined;
    public addName: string = undefined;
    public addUrl: string = undefined;

    public goMovie(movie: Movie) {
        this.navCtrl.navigateForward(['/movie', ], {queryParams: {id: movie.id}});
    }

    ngOnInit(): void {
        this.films = this.moviesService.getMovies();
    }
}
