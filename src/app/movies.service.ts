import {Injectable} from '@angular/core';
import {Movie} from './movie';
import {formatNumber} from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class MoviesService {
    private movies: Movie[] = [
        {
            id: 346,
            name: 'Titanic',
            urlPhoto: 'https://upload.wikimedia.org/wikipedia/ru/thumb/1/1b/Titanic_3D_Poster.jpg/274px-Titanic_3D_Poster.jpg'
        },
        {id: 242, name: 'Avatar', urlPhoto: 'https://likefilmdb.ru/static/images/film/1/319.jpg'},
        {
            id: 387,
            name: 'Terminator',
            urlPhoto: 'https://upload.wikimedia.org/wikipedia/ru/thumb/c/ca/Terminator_poster.jpg/272px-Terminator_poster.jpg'
        }
    ];

    public getMovies(): Movie[] {
        return this.movies;

    }

    public addMovies(id: number, name: string, urlPhoto: string) {
        this.movies.push({id, name, urlPhoto});
    }

    public deleteMovies(id: number) {
        let index: number;
        index = 0;
        while (index < this.movies.length) {
            if ((this.movies[index].id - id) === 0) {
                this.movies.splice(index, 1);
                break;
            }
            index++;
            if (index === this.movies.length) {
                alert('You entered an invalid ID!');
            }
        }
    }

}


