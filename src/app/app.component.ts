import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'app-root',
  template: '<ion-app></ion-app><ion-router-outlet></ion-router-outlet>',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

}
